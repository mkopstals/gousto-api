## Prerequisites
- Install docker https://docs.docker.com/install
## Run
- build dev

> make build

> make up

- build dev (without make)

> docker-compose build

> docker-compose up

- build prod

> make build-prod

> make up-prod

- build prod (without make)

> docker-compose -f docker-compose.prod.yml build

> docker-compose -f docker-compose.prod.yml up

## Testing dev API
> curl localhost:8080/api/recipes/

## Testing prod API
> curl 127.0.0.1/api/recipes/

## Test
- Run Tests
  > make test
## Improvements

1. The Pagination(api_gateway.api.utils) could use some abstraction, as it's taking up a fair amount of space and has to be applied manually to any query.  Ideally, I'd prefer at least the parsing bit to sit somewhere pre-method, say, dispatch.  Almost in middleware-like fashion.

2. You'll find I've wrapped the RecipeList resource return query in a marshal() method. It seems a bit redundant to wrap it around manually on every resource that would require it, so perhaps a decorator would do better.

3. The datetimes in api.forms and api.models are typed as strings, which is incorrect. Fixing this would require some fiddling  both inside of api.forms and api.models  as well some more complex code to read in from CSV.

## General thoughts

1. A typical read-only layer would likely have something akin to ElasticSearcch or Solr act as a query layer, one of which I probably would've picked in an otherwise read-only api assignment with a different kind of data source.

2. Forms in api_gateway.api.forms are meant to provide a degree of validation for recipe data
coming from an API client rather than the CSV.

3. api_gateway.ingest.data_store is a sort of filtering layer. Manipulating in-memory dicts manually on every possible combination of query fields(in the future) would be painful and unsafe, so it provides an interface to filter according to keys and values. Additionally, it manipulates instances of a validated dataclass model, which makes it easy to extend and ensures the dataset is uniform (no missing keys or wrongly typed values)

4. You'll notice I have written something akin to a model in api.models with the help of a dataclass. The reason for doing that being - reading in from CSV is inherently risky, and when storing data structures in memory, it's difficult to visualise or safely evaluate a schema. 

