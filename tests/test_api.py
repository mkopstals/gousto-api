import json
import unittest

from api_gateway.app import create_app
from tests.fixtures import recipe

class ApiTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client()
        self.recipe_root = '/api/recipes/'

    def test_recipe_list_filter(self):
        resp = self.client.get(f'{self.recipe_root}?recipe_cuisine=asian')
        data = resp.get_json()
        assert 'objects' in data

    def test_pagination_and_filter(self):
        resp = self.client.get(f'{self.recipe_root}?recipe_cuisine=asian&start=2')
        data = resp.get_json()
        assert len(data['objects']) == 1
        resp = self.client.get(f'{self.recipe_root}?recipe_cuisine=asian&start=1')
        data = resp.get_json()
        assert len(data['objects']) == 2

    def test_recipe_list_pagination(self):
        resp = self.client.get(f'{self.recipe_root}?start=9&limit=1')
        data = resp.get_json()
        assert 'objects' in data
        assert len(data['objects']) == 1

    def test_field_marshalling(self):
        resp = self.client.get(f'{self.recipe_root}?start=5&limit=7')
        data = resp.get_json()
        test_object = data['objects'][0]
        assert 'id' in test_object
        assert 'title' in test_object
        assert 'marketing_description' in test_object
        assert len(test_object) == 3

    def test_recipe_list_pagination_next(self):
        resp = self.client.get(f'{self.recipe_root}')
        data = resp.get_json()
        assert 'next' in data
        assert data['next'] == f'{self.recipe_root}?start=11&limit=10'

    def test_recipe_list_no_explit_pagination(self):
        resp = self.client.get(f'{self.recipe_root}')
        data = resp.get_json()
        assert len(data['objects']) == 10

    def test_recipe_update(self):
        initial_value = recipe['box_type']
        recipe['box_type'] = 'test_type'
        resp = self.client.put(
            f'{self.recipe_root}1',
            data=json.dumps(recipe),
            content_type='application/json'
        )
        data = resp.get_json()
        assert resp.status_code == 200
        assert data['box_type'] != initial_value

    def test_recipe_update_403(self):
        del recipe['carbs_grams']
        resp = self.client.put(
            f'{self.recipe_root}1',
            data=json.dumps(recipe),
            content_type='application/json',
        )
        data = resp.get_json()
        assert resp.status_code == 403
        assert 'carbs_grams' in data['message']

    def test_recipe_update_404(self):
        resp = self.client.put(
            '{self.recipe_root}123',
            data=json.dumps(recipe),
            content_type='application/json'
        )
        assert resp.status_code == 404

