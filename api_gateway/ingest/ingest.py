import csv

from api_gateway.api.models import Recipe

RECIPE_FIXTURES = '/app/api_gateway/ingest/fixtures/recipe-data.csv'

class DataStore:
    def __init__(self):
        self.data = []
        with open(RECIPE_FIXTURES) as csv_file:
            reader = csv.DictReader(csv_file)
            for row in reader:
                self.data.append(Recipe(**dict(row)))

    def _extract(self, records):
        return [record.asdict() for record in records]

    def _filter(self, extract=False, **kwargs):
        filtered_records = []
        for filter_type, filter_value in kwargs.items():
            if filter_value:
                if not filtered_records:
                    filtered_records = filter(lambda x: getattr(x, filter_type) == filter_value, self.data)
                else:
                    filtered_records = filter(lambda x: getattr(x, filter_type) == filter_value, matching_rec)
        if not kwargs:
            filtered_records = self.data
        if extract:
            return self._extract(filtered_records)
        else:
            return list(filtered_records)

    def get(self, recipe_id, extract=True):
        return next(iter(self._filter(extract=extract, id=recipe_id)), None)

    def filter_by(self, start=None, limit=None, **kwargs):
        matching_rec = self._filter(**kwargs, extract=True)
        total_count = len(matching_rec)
        if start:
            matching_rec = matching_rec[start:]
        if limit:
            matching_rec = matching_rec[:limit]
        return matching_rec, total_count

    def update(self, recipe_id, data):
        item = self.get(recipe_id, extract=False)
        if item:
            item.update(data)
            return item.asdict()

data_store = DataStore()
