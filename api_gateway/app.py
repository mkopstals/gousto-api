import logging
import os

from flask import Flask

def create_app():
    app = Flask(__name__)
    app.logger.setLevel(logging.INFO)
    config = os.environ.get('ENV')
    app.config.from_object('api_gateway.config.{}'.format(config))

    from api_gateway import api

    api.init(app)
    return app

