from flask_restful import reqparse

pagination_parser = reqparse.RequestParser()
pagination_parser.add_argument('start', type=int, location='args', default=1)
pagination_parser.add_argument('limit', type=int, location='args', default=10)

filter_parser = reqparse.RequestParser()
filter_parser.add_argument('recipe_cuisine', type=str, location='args', store_missing=False)


