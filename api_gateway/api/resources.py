import logging

from flask import abort, current_app, request
from flask_restful import (
    fields,
    marshal,
    Resource,
    reqparse,
    url_for
)

from api_gateway.api.forms import RecipeForm
from api_gateway.api.parsers import pagination_parser, filter_parser
from api_gateway.ingest.ingest import data_store
from api_gateway.api.utils import paginate

logger = logging.getLogger(__name__)

class RecipeResource(Resource):
    def get(self, recipe_id=None):
        recipe = data_store.get(recipe_id)
        if not recipe:
            return abort(404)
        return recipe

    def put(self, recipe_id):
        recipe = data_store.get(recipe_id)
        if not recipe:
            return abort(404)
        form = RecipeForm.from_json(request.get_json())
        if form.validate():
            form.id.process_data(recipe_id)
            return data_store.update(recipe_id, form.data), 200
        else:
            return abort(403, form.errors)



class RecipeListResource(Resource):
    resource_fields = {
        'id': fields.Integer,
        'title': fields.String,
        'marketing_description': fields.String
    }
    def get(self):
        pagination_args = pagination_parser.parse_args()
        start = pagination_args['start']
        limit = pagination_args['limit']
        filter_args = filter_parser.parse_args()
        filtered_recipes, total_count = data_store.filter_by(
            start=start - 1,
            limit=limit,
            **filter_args
        )
        next_url, prev_url = paginate(
            start,
            limit,
            self.endpoint,
            total_count,
            len(filtered_recipes)
        )
        return {
            'objects': marshal(filtered_recipes, self.resource_fields),
            'next': next_url,
            'prev': prev_url,
            'count': total_count
        }, 200

