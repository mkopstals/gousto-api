from datetime import datetime

from dataclasses import (
    asdict,
    dataclass,
    field,
    fields,
    replace
)


@dataclass
class Recipe:
    id: int
    created_at: str
    updated_at: str
    box_type: str
    title: str
    slug: str
    short_title: str
    marketing_description: str
    calories_kcal: int
    protein_grams: int
    fat_grams: int
    carbs_grams: int
    bulletpoint1: str
    bulletpoint2: str
    bulletpoint3: str
    recipe_diet_type_id: str
    season: str
    base: str
    protein_source: str
    preparation_time_minutes: int
    shelf_life_days: int
    equipment_needed: str
    origin_country: str
    recipe_cuisine: str
    in_your_box: str
    gousto_reference: int

    def __post_init__(self):
        for field in fields(self):
            value = getattr(self, field.name)
            if not isinstance(value, field.type):
                setattr(self, field.name, field.type(value))

    def update(self, data):
        for attr, value in data.items():
            setattr(self, attr, value)

    def asdict(self):
        return asdict(self)
