from flask_restful import Resource, Api
from api_gateway.api.resources import RecipeListResource, RecipeResource

def init(app):
    api = Api(app, prefix='/api')
    api.add_resource(RecipeResource, '/recipes/<int:recipe_id>', endpoint='recipe')
    api.add_resource(RecipeListResource, '/recipes/', endpoint='recipe_list')
