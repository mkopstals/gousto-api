from wtforms import Form
from wtforms.fields import DateTimeField, IntegerField, TextField
from wtforms.validators import InputRequired
import wtforms_json

wtforms_json.init()

class RecipeForm(Form):
    id = IntegerField()
    created_at = TextField()
    updated_at = TextField()
    box_type = TextField(
        validators=[InputRequired()]
    )
    title = TextField()
    slug = TextField(
        validators=[InputRequired()]
    )
    short_title = TextField()
    marketing_description = TextField()
    calories_kcal = IntegerField(
        validators=[InputRequired()]
    )
    protein_grams = IntegerField(
        validators=[InputRequired()]
    )
    fat_grams = IntegerField(
        validators=[InputRequired()]
    )
    carbs_grams = IntegerField(
        validators=[InputRequired()]
    )
    bulletpoint1 = TextField()
    bulletpoint2 = TextField()
    bulletpoint3 = TextField()
    recipe_diet_type_id = TextField(
        validators=[InputRequired()]
    )
    season = TextField()
    base = TextField()
    protein_source = TextField(
        validators=[InputRequired()]
    )
    preparation_time_minutes = IntegerField()
    shelf_life_days = IntegerField(
        validators=[InputRequired()]
    )
    equipment_needed = TextField()
    origin_country = TextField()
    recipe_cuisine = TextField(
        validators=[InputRequired()]
    )
    in_your_box = TextField()
    gousto_reference = IntegerField(
        validators=[InputRequired()]
    )
