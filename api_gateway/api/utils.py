from flask_restful import url_for

def paginate(start, limit, url, total_count, filtered_count):
    if filtered_count + start < total_count:
        pagination_dict = {
            'start': start + limit,
            'limit': limit
        }
        next_url = url_for(url, **pagination_dict)
    else:
        next_url = None
    if start > 1:
        pagination_dict = {
            'start': max(1, start - limit),
            'limit': limit
        }
        prev_url = url_for(url, **pagination_dict)
    else:
        prev_url = None
    return next_url, prev_url


