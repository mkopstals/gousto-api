FROM python:3.7.3

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

COPY . /app

ENTRYPOINT ["flask"]

ENV FLASK_APP /app/api_gateway/run.py

CMD ["run", "--host=0.0.0.0", "--port=8080"]

