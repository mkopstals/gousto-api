.PHONY: help
.DEFAULT_GOAL := help

help:
		@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

build:
		docker-compose -f docker-compose.yml build

up:
		docker-compose -f docker-compose.yml up

build-prod:
		docker-compose -f docker-compose.prod.yml build

up-prod:
		docker-compose -f docker-compose.prod.yml up

test:
		docker-compose run -e ENV=test --entrypoint "python -m pytest -vv" web
